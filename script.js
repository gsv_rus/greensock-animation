window.addEventListener('DOMContentLoaded', function () {

  var tl = gsap.timeline({ paused: false });

  tl.from(".hero__left", { duration: 0.8, opacity: 0, y: 300 })
  tl.from(".hero__descr", { duration: 2.5, opacity: 0, })
  tl.from(".photos__author", { duration: 0.8, opacity: 0 }, "-=2")
  tl.from(".photos-wrap-1", { duration: 0.8, opacity: 0, scale: 0.65 }, "-=3")
  tl.from(".photos-wrap-2", { duration: 0.8, opacity: 0, scale: 0.65 }, "-=2.5")
  tl.from(".photos-wrap-3", { duration: 0.8, opacity: 0, scale: 0.65 }, "-=2.2")

  var open = document.getElementById("burger");
  var close = document.getElementById("close");

  var tlB = gsap.timeline({ paused: true });

  tlB.fromTo(".menu", { display: "none", opacity: 0, y: 100 }, { display: "block", opacity: 1, y: 0 })
  tlB.from(".menu-bottom", { duration: 0.8, opacity: 0, y: 100 })
  tlB.from(".menu__top", { duration: 0.6, opacity: 0, y: -100 }, "-=0.6")
  tlB.from(".menu__nav", { duration: 0.4, opacity: 0, y: 50 }, "-=1")
  tlB.from(".menu__right", { duration: 0.8, opacity: 0, y: 50, }, "-=0.2")
  tlB.from(".social", { duration: 0.8, opacity: 0, y: 50, }, "-=0.9")

  open.onclick = function () {
    tlB.play();
  }

  close.onclick = function () {
    tlB.reverse();
  }
})
